use bytes::Bytes;
use bencoder;

#[test]
fn dict_list_test() {
    // vector for holding different test cases
    let mut inputs = Vec::new();
    // samples that contain both dictionaries and lists
    inputs.push(Bytes::from(&b"d9:Breakfastl4:Eggs5:Toast6:Yogurt5:Juiceee"[..]));
    inputs.push(Bytes::from(&b"l4:Todo11:Call Officed4:Shopld6:Fruitsl6:Apples7:Orangeseed3:Vegl7:Peppers6:Onionseeeee"[..]));
    // for each input
    for byte_input in inputs {
        // create new object, and set instructions to byte_input
        let mut my_file = bencoder::Parsed::new(byte_input);
        // decode the instructions
        let byte_output = my_file.decode();
        // match based on the result
        match byte_output {
                Ok(_) => println!("{}", my_file),
                Err(e) => panic!("Error: {} with valid input.", e),
        }
    }
}

use std::fs;
use std::io;
use std::io::prelude::*;

#[test]
fn test_files() {
    let mut files = Vec::with_capacity(2);
    // open and read the files as [u8]
    let manjaro = fs::read("files/manjaro.torrent").expect("Couldn't open Manjaro file");
    let ubuntu = fs::read("files/ubuntu.torrent").expect("Couldn't open Ubuntu file");
    // convert to Bytes and push to vector
    files.push((Bytes::from(manjaro), "Manjaro"));
    files.push((Bytes::from(ubuntu), "Ubuntu"));
    // for each file
    for (file, name) in files {
        let mut my_file = bencoder::Parsed::new(file);
        let byte_output = my_file.decode();
        match byte_output {
            Ok(_) => println!("Success with {} file", name),
            Err(e) => panic!("Failure on file {} with error: {}", name, e.to_string()),
        }
    }
}
