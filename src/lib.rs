use bytes::Bytes;
use std::vec::Vec;
use std::collections::BTreeMap;
use std::fmt;
use std::str::Utf8Error;
use std::num::ParseIntError;
use std::convert::From;
use std::cmp::Ordering;

// wrapper enum, to let other parts know
// whether or not the bytestring is safely converted to utf8
// derives Clone for entering as btreemap keys
#[derive(Clone)]
pub enum ByteString {
    SafeByteString(Bytes),
    UnsafeByteString(Bytes),
}

// I know these could have been derived,
// but I didn't have practice with implementing these so I felt
// that it would be good to do.

// might replace with derivations later
impl PartialEq for ByteString {
    fn eq(&self, other: &Self) -> bool {
        use ByteString::*;

        match (self, other) {
            (SafeByteString(a), SafeByteString(b)) => a == b,
            (SafeByteString(a), UnsafeByteString(b)) => a == b,
            (UnsafeByteString(a), UnsafeByteString(b)) => a == b,
            _ => false,
        }
    }
}

impl Eq for ByteString {}

impl Ord for ByteString {
    fn cmp(&self, other: &Self) -> Ordering {
        use ByteString::*;

        match (self, other) {
            (SafeByteString(a), SafeByteString(b)) => a.cmp(b),
            (SafeByteString(a), UnsafeByteString(b)) => a.cmp(b),
            (UnsafeByteString(a), SafeByteString(b)) => a.cmp(b),
            (UnsafeByteString(a), UnsafeByteString(b)) => a.cmp(b),
        }
    }
}

impl PartialOrd for ByteString {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// deconstructs the bytestrings to use the Bytes len() method
impl ByteString {
    fn len(&self) -> usize {
        match self {
            ByteString::SafeByteString(bytstr)      => bytstr.len(),
            ByteString::UnsafeByteString(ubytstr)   => ubytstr.len(),
        }
    }
}


// for printing our different bytestrings
// a distiction is used, since from_utf8 is faster,
// but from_utf8_lossy is required for unsafe bytes
impl fmt::Display for ByteString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            ByteString::SafeByteString(bytstr)      => write!(f, "{}", std::str::from_utf8(bytstr).unwrap()),
            ByteString::UnsafeByteString(ubytstr)   => write!(f, "{}", String::from_utf8_lossy(ubytstr).to_string()),
        }
    }
}

// different parsed options for data
// dictionaries and lists can contain each other
pub enum ParseOptions {
    Integer(i64),
    BString(ByteString),
    Dictionary(BTreeMap<ByteString, ParseOptions>),
    List(Vec<ParseOptions>),
}

// formatting for the parsed options
impl fmt::Display for ParseOptions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            ParseOptions::Integer(int)              => write!(f, "{}", int),
            ParseOptions::BString(byte_string)      => write!(f, "{}", byte_string),
            ParseOptions::List(list)                => Ok(self.fmt_list(f, list, 0)),
            ParseOptions::Dictionary(dict)          => Ok(self.fmt_dict(f, dict, 0)),
        }
    }
}

// formatting help for lists and dictionaries,
// since each have recursive capability
impl ParseOptions {
    // recursive list formatting
    fn fmt_list(&self, f: &mut fmt::Formatter<'_>, l: &[ParseOptions], space: usize) {
        for ele in l {
            for _ in 0..space {write!(f, " ");}
            match ele {
                ParseOptions::List(list) =>
                    if list.is_empty() {
                        writeln!(f, "<Empty List>");
                    } else if list.len() == 1 {
                        if let Some(E) = list.get(0) {
                            writeln!(f, " * {}", E);
                        }
                    } else {
                        self.fmt_list(f, list, space + 1);
                    },
                ParseOptions::Dictionary(dict) =>
                    if dict.is_empty() {
                        writeln!(f, "* <Empty Dictionary>");
                    } else if dict.len() == 1 {
                        for (key, val) in dict {writeln!(f, "* {}: {}", key, val);}
                    } else {
                        write!(f, "*");
                        self.fmt_dict(f, dict, space + 2);
                    },
                _ => {writeln!(f, "* {}", ele); ()},
            }
        }
    }
    // recursive dictionary formatting
    fn fmt_dict(&self, f: &mut fmt::Formatter<'_>, d: &BTreeMap<ByteString, ParseOptions>, space: usize) {
        for _ in 0..space {write!(f, " ");}
        writeln!(f, "{{");
        let space = space + 2;
        if d.len() == 1 {
            for _ in 0..space {write!(f, " ");}
            for (key, val) in d {write!(f, "{}: {}", key, val);}
            for _ in 0..space - 2 {write!(f, " ");}
            writeln!(f, "}}");
        }
        else {
            for (key, val) in d {
                for _ in 0..space {write!(f, " ");}
                write!(f, "{}:", key);
                match val {
                    ParseOptions::List(list) => {
                        if list.is_empty() {
                            writeln!(f, " <Empty List>");
                        } else if list.len() == 1 {
                            if let Some(E) = list.get(0) {
                                writeln!(f, " * {}", E);
                            }
                        } else {
                            if let Some(E) = list.get(0) {
                                writeln!(f, " * {}", E);
                            }
                            self.fmt_list(f, &list[1..], space + key.len() + 2);
                        }
                    },
                    ParseOptions::Dictionary(dict) => {
                        if dict.is_empty() {
                            writeln!(f, " <Empty Dictionary>");
                        } else {
                            self.fmt_dict(f, dict, space + key.len() + 6);
                        }
                    },
                    _ => {writeln!(f, " {}", val);},
                }
            }
            for _ in 0..space - 2 {write!(f, " ");} 
            writeln!(f, "}}");
        }
    }   
}

// Error options
// SliceErr comes from .get() use, and contains the index that the error occurred in
// InvalidData comes from a couple of areas, namely elements appearing where they shouldn't
    // (a non-number inbetween an 'i' and an 'e', for example)
    // contains the index of the error
// UTF8Err comes from parsing Bytes to strings
    // mostly used in part when converting Bytes to strings to ints
    // contains the error index
// ParseErr comes from parsing a string to an int
    // occurs in the decode_integer method
    // contains the error index
pub enum ParseError {
    SliceErr(usize),
    InvalidData(usize),
    UTF8Err(usize, Utf8Error),
    ParseErr(usize, ParseIntError),
}

// fancy error printing
impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            ParseError::SliceErr(index)         => write!(f, "Couldn't Get Slice at Index: {}", index),
            ParseError::InvalidData(index)      => write!(f, "Invalid Input Data at Index: {}", index),
            ParseError::UTF8Err(index, error)   => write!(f, "Failure to Convert from UTF8 at index {} with error: {}", index, error),
            ParseError::ParseErr(index, error)  => write!(f, "Failure to Parse String at index {} with error: {}", index, error),
        }
    }
}

// derivations from existing error types
impl From<Utf8Error> for ParseError {
    fn from(error: Utf8Error) -> Self {
        ParseError::UTF8Err(0, error)
    }
}

impl From<ParseIntError> for ParseError {
    fn from(error: ParseIntError) -> Self {
        ParseError::ParseErr(0, error)
    }
}

// main struct for holding the data
// instructions contains the original Bytes of the file,
// while structure contains the parsed data
pub struct Parsed {
    instructions: Bytes,
    pub structure: Vec<ParseOptions>,
}

// for printing the structure
impl fmt::Display for Parsed {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // for each element in the structure, in order
        for element in &self.structure {
            writeln!(f, "{}", element);
        }
        Ok(())
    }
}

// methods for Parsed struct
impl Parsed{
    // constructor, takes the original Bytes and throws it at instructions
    pub fn new(input: Bytes) -> Parsed {
        Parsed {
            instructions: input,
            structure: Vec::new(),
        }
    }

    // public method for decoding the data
    pub fn decode(&mut self) -> Result<(), ParseError>{
        // total input length
        let length: usize = self.instructions.len();
        // counter for walking through torrent bytes
        let mut walk: usize = 0;
        while walk < length {
            match self.instructions.get(walk) {
                Some(b'i') => match self.decode_integer(&self.instructions.slice(walk..)) {
                    Ok((length, ret_int)) => {walk += length + 2;
                        self.structure.push(ParseOptions::Integer(ret_int));},
                    Err(e) => return Err(self.match_errors(e, walk)),
                },
                Some(b'0'..=b'9') => match self.decode_string(&self.instructions.slice(walk..)) {
                    Ok((length, ret_str)) => {walk += length;
                        self.structure.push(ParseOptions::BString(ret_str));},
                    Err(e) => return Err(self.match_errors(e, walk)),
                },
                Some(b'l') => match self.decode_list(&self.instructions.slice(walk..)) {
                    Ok((length, ret_list)) => {walk += length;
                        self.structure.push(ParseOptions::List(ret_list));},
                    Err(e) => return Err(self.match_errors(e, walk)),
                },
                Some(b'd') => match self.decode_dict(&self.instructions.slice(walk..)) {
                    Ok((length, ret_dict)) => {walk += length;
                        self.structure.push(ParseOptions::Dictionary(ret_dict));},
                    Err(e) => return Err(self.match_errors(e, walk)),
                },
                Some(_) => return Err(ParseError::InvalidData(walk)),
                None => return Err(ParseError::SliceErr(walk)),
            }
        }
        Ok(())
    }

    // getter for 
    pub fn get(&self) -> &Vec<ParseOptions> {
        &self.structure
    }

    // helper method for matching errors, and returning appropriate error index
    fn match_errors(&mut self, error: ParseError, index: usize) -> ParseError {
        match error {
            ParseError::InvalidData(pwalk)  => ParseError::InvalidData(index + pwalk),
            ParseError::SliceErr(pwalk)     => ParseError::SliceErr(index + pwalk),
            ParseError::UTF8Err(pwalk, e)   => ParseError::UTF8Err(index + pwalk, e),
            ParseError::ParseErr(pwalk, e)  => ParseError::ParseErr(index + pwalk, e),
        }
    }

    fn decode_integer(&mut self, integer: &Bytes) ->  Result<(usize, i64), ParseError> {
        match integer.get(0) {
            // if our first character is 'i'
            Some(b'i') => {match integer.get(1) {
                    // if the second char is "-"
                    // check for it being postceeded by 0 or e
                    // if it is, return an error
                    Some(b'-') => match integer.get(2) {
                        Some(b'0') | Some(b'e') => return Err(ParseError::InvalidData(2)),
                        Some(_) => (),
                        None => return Err(ParseError::SliceErr(2)),
                    },
                    // if the second char is "0"
                    Some(b'0') => match integer.get(2) {
                        // if it's 'i0e', return length 3, and number 0
                        Some(b'e') => return Ok((3, 0)),
                        // throw an error for anything that isn't 'e'
                        Some(_) => return Err(ParseError::InvalidData(2)),
                        None => return Err(ParseError::SliceErr(2)),
                    },
                    // if our string has 'i' followed by 'e', throw error
                    Some(b'e') => return Err(ParseError::InvalidData(1)),
                    // if it's a number in range, it's okay
                    Some(b'1'..=b'9') => (),
                    // Anything else is an error
                    Some(_) => return Err(ParseError::InvalidData(1)),
                    None => return Err(ParseError::SliceErr(1)),
                }
                // walk until an error, or until we hit 'e'
                // not done walking
                let mut done = false;
                // counter for digits
                // should only be 63 binary digits long, maximum
                let mut digits: usize = 0;
                // walk
                while !done && digits < 20 {
                    match integer.get(digits + 2) {
                        // increment number of digits, if valid digit
                        Some(b'0'..=b'9') => digits += 1,
                        Some(b'e')        => done = true,
                        Some(_)           => return Err(ParseError::InvalidData(digits + 2)),
                        None              => return Err(ParseError::SliceErr(digits + 2)),
                    }
                }
                // when done, convert it into a number and return it
                if let Some(number) = integer.get(1..digits+2) {
                    Ok((digits+3, std::str::from_utf8(number)?.parse::<i64>()?))
                } else {
                    Err(ParseError::SliceErr(digits + 2))
                }},
            Some(_) => Err(ParseError::InvalidData(0)),
            // invalid index
            None => Err(ParseError::SliceErr(0)),
        }
    }

    fn decode_string(&mut self, bytstr: &Bytes) ->  Result<(usize, ByteString), ParseError> {
        // ensure that this is a byte string
        match bytstr.get(0) {
            // if first char is 0, then next has to be ':', else invalid
            Some(b'0') => match bytstr.get(1) {
                Some(b':') => return Ok((2, ByteString::SafeByteString(Bytes::from("")))),
                Some(_) => return Err(ParseError::InvalidData(1)),
                None => return Err(ParseError::SliceErr(1)),
            },
            // if non-zero, keep going to get the length
            Some(b'1'..=b'9') => (),
            // any invalid char or 'None' is invalid
            Some(_) => return Err(ParseError::InvalidData(0)),
            None => return Err(ParseError::SliceErr(0))
        }
        // keep going until ':'
        let mut digits = 0;
        loop {
            match bytstr.get(digits) {
                Some(b':') => break,
                Some(b'0'..=b'9') => digits += 1,
                Some(_) => return Err(ParseError::InvalidData(digits)),
                None => return Err(ParseError::SliceErr(digits)),
            }
        }
        // get all Bytes until the ':' as a [u8],
        if let Some(s) = bytstr.get(0..digits) {
            // convert to a string, then parse to a number
            let string_dig = std::str::from_utf8(s)?.parse::<usize>()?;
            // if we have a valid slice
            if let Some(b) = bytstr.get(digits + 1..digits + 1 + string_dig) {
                // check for non Utf8 chars
                if let Ok(_) = std::str::from_utf8(b) {
                    // if all valid, return the safe option
                    Ok((digits + string_dig + 1, ByteString::SafeByteString(bytstr.slice(digits + 1..digits + 1 + string_dig))))
                } else {
                    // else invalid chars, so return unsafe option
                    Ok((digits + string_dig + 1, ByteString::UnsafeByteString(bytstr.slice(digits + 1..digits + 1 + string_dig))))
                }
            } else {
                // invalid slice (probably out of bounds)
                Err(ParseError::SliceErr(digits + 1 + string_dig))
            }
        } else {
            // invalid slice (probably out of bounds)
            Err(ParseError::SliceErr(digits))
        }
    }

    fn decode_list(&mut self, list: &Bytes) ->  Result<(usize, Vec<ParseOptions>), ParseError> {
        let mut temp_list = Vec::new();
        // match characters
        // call other methods if you encounter the specific delimiters
        match list.get(0) {
            Some(b'l') => {
                let mut walk = 1;
                loop {
                    match list.get(walk) {
                        Some(b'e') => return Ok((walk + 1, temp_list)),
                        Some(b'0'..=b'9') => match self.decode_string(&list.slice(walk..)) {
                            Ok((length, byt_str)) => {walk += length;
                                temp_list.push(ParseOptions::BString(byt_str));
                            },
                            Err(e) => return Err(self.match_errors(e, walk)),
                        },
                        Some(b'i') => match self.decode_integer(&list.slice(walk..)) {
                            Ok((length, number)) => {walk += length;
                                temp_list.push(ParseOptions::Integer(number));},
                            Err(e) => return Err(self.match_errors(e, walk)),
                        },
                        Some(b'd') => match self.decode_dict(&list.slice(walk..)) {
                            Ok((length, ret_dict)) => {walk += length;
                                temp_list.push(ParseOptions::Dictionary(ret_dict));},
                            Err(e) => return Err(self.match_errors(e, walk)),
                        },
                        Some(b'l') => match self.decode_list(&list.slice(walk..)) {
                            Ok((length, ret_list)) => {walk += length;
                                temp_list.push(ParseOptions::List(ret_list));},
                            Err(e) => return Err(self.match_errors(e, walk)),
                        },
                        Some(_) => return Err(ParseError::InvalidData(walk)),
                        None => return Err(ParseError::SliceErr(walk)),
                    }
                }
            },
            Some(_) => Err(ParseError::InvalidData(0)),
            None => Err(ParseError::SliceErr(0)),
        }
    }

    fn decode_dict(&mut self, dict: &Bytes) ->  Result<(usize, BTreeMap<ByteString, ParseOptions>), ParseError> {
        // can hold everything
        // keys must be strings
        match dict.get(0) {
            Some(b'd') => {
                // make new BTreeMap
                let mut temp_dict = BTreeMap::new();
                // alternating boolean that tells us when to insert a key-value pair
                let mut value = false;
                // counter var
                let mut walk = 1;
                // blank key
                let mut key = ByteString::SafeByteString(Bytes::from(""));
                // loop getting key value pairs
                loop {
                    match dict.get(walk) {
                        // when you hit 'e', return the dictionary
                        Some(b'e') => return Ok((walk + 1, temp_dict)),
                        Some(_) => {
                            // if looking for the key, get a string
                            if !value {
                                match self.decode_string(&dict.slice(walk..)) {
                                    Ok((length, byte_string)) => {
                                        walk += length;
                                        key = byte_string;
                                    },
                                    Err(e) => return Err(self.match_errors(e, walk)),
                                }
                                value = true;
                            } else {
                                match dict.get(walk) {
                                    Some(b'0'..=b'9') => match self.decode_string(&dict.slice(walk..)) {
                                        Ok((length, byt_str)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::BString(byt_str));},
                                        Err(e) => return Err(self.match_errors(e, walk)),
                                    },
                                    Some(b'i') => match self.decode_integer(&dict.slice(walk..)) {
                                        Ok((length, number)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::Integer(number));},
                                        Err(e) => return Err(self.match_errors(e, walk)),
                                    },
                                    Some(b'l') => match self.decode_list(&dict.slice(walk..)) {
                                        Ok((length, ret_list)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::List(ret_list));},
                                        Err(e) => return Err(self.match_errors(e, walk)),
                                    },
                                    Some(b'd') => match self.decode_dict(&dict.slice(walk..)) {
                                        Ok((length, ret_dict)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::Dictionary(ret_dict));},
                                        Err(e) => return Err(self.match_errors(e, walk)),
                                    },
                                    Some(_) => return Err(ParseError::InvalidData(walk)),
                                    None => return Err(ParseError::SliceErr(walk)),
                                }
                                value = false;
                            }
                        },
                        None => return Err(ParseError::SliceErr(walk)),
                    }
                }
            },
            Some(_) => Err(ParseError::InvalidData(0)),
            None => Err(ParseError::SliceErr(0)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn string_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // different tests
        inputs.push((Bytes::from(&b"5:hello"[..]), true, Bytes::from("hello")));
        inputs.push((Bytes::from(&b"3:hey"[..]), true, Bytes::from("hey")));
        inputs.push((Bytes::from(&b"10:this works"[..]), true, Bytes::from("this works")));
        inputs.push((Bytes::from(&b"0:"[..]), true, Bytes::from("")));
        inputs.push((Bytes::from(&b"05:hello"[..]), false, Bytes::from("")));
        inputs.push((Bytes::from(&b"20:hi"[..]), false, Bytes::from("")));
        inputs.push((Bytes::from(&b"1:"[..]), false, Bytes::from("")));
        inputs.push((Bytes::from(&b"-0:"[..]), false, Bytes::from("")));
        // for each input
        // has the input, whether the test is valid,
        // the expected output, and the expected length of the output
        for (byte_input, valid, expect_out) in inputs {
            // create a new bencoder object
            let mut my_file = Parsed::new(byte_input);
            // decode (parse) the instructions
            let byte_output = my_file.decode();
            // if the test is valid
            if valid {
                match byte_output {
                    // with valid output
                    Ok(_) => match my_file.structure.get(0) {
                        // we should have a byte string, and can assert it equal to our expected output
                        Some(ParseOptions::BString(ByteString::SafeByteString(b))) => assert_eq!(*b, expect_out),
                        // should only be the above case, so panic otherwise
                        _ => panic!("Invalid Type"),
                    },
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                match byte_output {
                    // with valid output
                    Ok(_) => panic!("Invalid Output"),
                    Err(_) => (),
                }
            }
        }
    }

    #[test]
    fn int_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // tests
        inputs.push((Bytes::from(&b"i0e"[..]), true, 0));
        inputs.push((Bytes::from(&b"i2022e"[..]), true, 2022));
        inputs.push((Bytes::from(&b"i-14e"[..]), true, -14));
        inputs.push((Bytes::from(&b"i92034923049923e"[..]), true, 92034923049923 as i64));
        inputs.push((Bytes::from(&b"ie"[..]), false, 0));
        inputs.push((Bytes::from(&b"i-0e"[..]), false, 0));
        inputs.push((Bytes::from(&b"i-e"[..]), false, 0));
        inputs.push((Bytes::from(&b"i01e"[..]), false, 0));
        // for each input
        for (byte_input, valid, expect_out) in inputs {
            // make new bencoder object
            let mut my_file = Parsed::new(byte_input);
            // parse the instructions
            let byte_output = my_file.decode();
            // test validity
            if valid {
                // if the parsing was valid
                match byte_output {
                    // if we were returned the appropriate result
                    // check that result for validity
                    Ok(_) => match my_file.structure.get(0) {
                        Some(ParseOptions::Integer(i)) => assert_eq!(i, &expect_out),
                        _ => panic!("Invalid Type"),
                    },
                    // gave Err when should have given Ok
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                // if invalid
                match byte_output {
                    Err(_) => (),
                    Ok(_) => panic!("Invalid Output"),
                }
            }
        }
    }

    #[test]
    fn list_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // test inputs
        inputs.push((Bytes::from(&b"l12:Grocery List4:Eggs4:Milk5:Breade"[..]), true));
        inputs.push((Bytes::from(&b"l7:Numbers3:Onei2ei3e4:Foure"[..]), true));
        inputs.push((Bytes::from(&b"l6:Silver3:And4:Gold5:Never3:Get3:Olde"[..]), true));
        inputs.push((Bytes::from(&b"le"[..]), true));
        inputs.push((Bytes::from(&b"l1:Electronics6:Laptop5:Phonele"[..]), false));
        inputs.push((Bytes::from(&b"lle"[..]), false));
        inputs.push((Bytes::from(&b"l"[..]), false));
        inputs.push((Bytes::from(&b"l6:Invalide"[..]), false));

        for (byte_input, valid) in inputs {
            // parse into new object
            let mut my_file = Parsed::new(byte_input);
            // decode the instructions
            let byte_output = my_file.decode();
            if valid {
                match byte_output {
                    Ok(_) => match my_file.structure.get(0) {
                        Some(ParseOptions::List(_)) => (),
                        _ => panic!("Invalid Type"),
                    },
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                // if invalid
                match byte_output {
                    Err(_) => (),
                    Ok(_) => panic!("Invalid Output"),
                }
            }
            
        }
    }

    #[test]
    fn dict_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // test inputs
        inputs.push((Bytes::from(&b"d4:frog9:amphibian3:cat6:felinee"[..]), true));
        inputs.push((Bytes::from(&b"de"[..]), true));
        inputs.push((Bytes::from(&b"d3:onei1e3:twoi2e5:threei3ee"[..]), true));
        inputs.push((Bytes::from(&b"d0:5:empty1:14:fulle"[..]), true));
        inputs.push((Bytes::from(&b"di1e3:onei2e3:two"[..]), false));
        inputs.push((Bytes::from(&b"dde"[..]), false));
        inputs.push((Bytes::from(&b"dde7:invalide"[..]), false));
        inputs.push((Bytes::from(&b"d3:one5:validi2e7:invalide"[..]), false));
        for (byte_input, valid) in inputs {
            // parse into new object
            let mut my_file = Parsed::new(byte_input);
            // decode the instructions
            let byte_output = my_file.decode();
            if valid {
                match byte_output {
                    Ok(_) => match my_file.structure.get(0) {
                        Some(ParseOptions::Dictionary(_)) => (),
                        _ => panic!("Invalid Type"),
                    },
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                // if invalid
                match byte_output {
                    Err(_) => (),
                    Ok(_) => panic!("Invalid Output"),
                }
            }
            
        }
    }
}
