# Bencoder

A bencoding parser written in Rust.

## Usage

A new bencoder object takes a sequence of Bytes as its parameter:

```rust
let sample_input = Bytes::from(&b"6:sample5:input"[..]);
let my_output = bencoder::Parsed::new(sample_input);
```

And is then decoded:

```rust
my_output.decode();
```

The decoded Bytes yield a recursive Rust struct, which holds the parsed data in the order it appeared.

```rust
println!("{}", my_output);
// prints
sample
input
```

This struct is also formatted to print recursive dictionaries and lists:

```rust
let byte_str = b"l2:hil3:heyl5:theree2:ite2:med4:bean4:milk6:burger3:dog3:haml3:big3:eggeee";
let sample_input = Bytes::from(&byte_str[..]);
let my_output = bencoder::Parsed::new(sample_input);
my_output.decode();
println!("{}", my_output);
// prints
* hi
 * hey
  * there
 * it
* me
* {
   bean: milk
   burger: dog
   ham: * big
        * egg
 }
```

## Bencode Specification

### Integers

* Preceeded by `i`, postceeded by `e`
  * Ex. `i17e`
* Negatives have a `-` before the number characters
  * Ex. `i-93e`
* Negative 0 (`i-0e`) is invalid
* Leading 0's are invalid (`i03e`)

### Byte String

* Encoded in the form `<length>:<contents>`
  * Ex. `3:hey`
* Not necessarily characters
* Can have the empty string (`0:`)

### List

* Preceeded by `l`, postceeded by `e`
  * Ex. `l2:hii88ee`
    * A list that contains `hi` and `88`
* The empty list is `le`

### Dictionary

* Preceeded by `d`, postceeded by `e`
  * Ex. `d4:frog9:amphibian3:cat6:felinee`
    * A dictionary with the following key value pairs:
      * { "frog": "amphibian",
      * "cat": "feline" }
* Dictionary must be in sorted order (binary comparison)
* The empty dictionary is `de`
